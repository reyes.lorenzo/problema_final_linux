#!/bin/bash

# Recopilar datos de un API

mkdir data

cd data

wget https://datahub.io/core/population/r/population.csv

# Limpiar el conjunto de datos: Filtrar por país “Spain”

grep 'Spain' population.csv | sed 's/,/\t/g' > spain_data.tsv

#  Visualizar el resultado: Una gráfica de la población en función del año

gnuplot -e "set terminal dumb; plot 'spain_data.tsv' using 3:4"
